package com.integrado.churrascometro

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCalcular.setOnClickListener {
            val intent = Intent(this, ResultadoActivity::class.java)

//            Recupera o numero de convidados
            val numHomem = if (editHomem.text.toString() != "") editHomem.text.toString() else "0"
            val numMulher = if (editMulher.text.toString() != "") editMulher.text.toString() else "0"
            val numCrianca = if (editCrianca.text.toString() != "") editCrianca.text.toString() else "0"

//            Inicializa as variaveis de quantidade de Alimentos para o churrasco
            var qtdPicanha = 0
            var qtdMaminha = 0
            var qtdLinguica = 0
            var qtdFrango = 0
            var qtdArroz = 0
            var qtdBatata = 0
            var qtdFarofa = 0
            var qtdPaes = 0.0

//            Varivel contador
            var i = 0

//            Adiciona às variaveis o consumo de Alimentos dos homens informados
            while (i < numHomem.toInt()) {
                qtdPicanha += 240
                qtdMaminha += 80
                qtdLinguica += 40
                qtdFrango += 40
                qtdArroz += 100
                qtdBatata += 200
                qtdFarofa += 100
                qtdPaes += 1.5
                i++
            }
//           Zera a variavel contador
            i = 0
            //            Adiciona as variaveis o consumo de Alimentos das mulheres informadas
            while (i < numMulher.toInt()) {
                qtdPicanha += 180
                qtdMaminha += 60
                qtdLinguica += 30
                qtdFrango += 30
                qtdArroz += 80
                qtdBatata += 150
                qtdFarofa += 80
                qtdPaes += 1.5
                i++
            }
            i = 0
            //            Adiciona as variaveis o consumo de Alimentos das crianças informadas
            while (i < numCrianca.toInt()) {
                qtdPicanha += 120
                qtdMaminha += 40
                qtdLinguica += 20
                qtdFrango += 20
                qtdBatata += 100
                i++
            }

//            Adiciona as variaveis na Intent do Resultado
            intent.putExtra("picanha", qtdPicanha.toString())
            intent.putExtra("maminha", qtdMaminha.toString())
            intent.putExtra("linguica", qtdLinguica.toString())
            intent.putExtra("frango", qtdFrango.toString())
            intent.putExtra("arroz", qtdArroz.toString())
            intent.putExtra("batata", qtdBatata.toString())
            intent.putExtra("farofa", qtdFarofa.toString())
            intent.putExtra("pao", qtdPaes.toString())
//          Inicia a activity
            startActivity(intent)
        }
    }
}
