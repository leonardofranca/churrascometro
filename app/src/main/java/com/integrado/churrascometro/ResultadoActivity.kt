package com.integrado.churrascometro

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_resultado.*

class ResultadoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)

        val dados: Bundle = intent.extras

//        Recupera os valores vindo da Main Activity
        val picanha = dados.getString("picanha")
        val maminha = dados.getString("maminha")
        val linguica = dados.getString("linguica")
        val frango = dados.getString("frango")
        val arroz = dados.getString("arroz")
        val batata = dados.getString("batata")
        val farofa = dados.getString("farofa")
        val pao = dados.getString("pao")

//        Seta nos TextView os valores
        qtdPicanha.setText(picanha + " g de Picanha")
        qtdMaminha.setText(maminha + " g de Maminha")
        qtdLinguica.setText(linguica + " g de Linguiça")
        qtdFrango.setText(frango + " g de Frango")
        qtdArroz.setText(arroz + " g de Arroz")
        qtdBatata.setText(batata + " g de Batata")
        qtdFarofa.setText(farofa + " g de Farofa")
        qtdPao.setText(pao + " Pães")

//        Botao Fechar Resultado
        btnFechar.setOnClickListener {
            finish()
        }
    }
}
